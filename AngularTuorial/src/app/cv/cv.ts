export type CVDATA = {
    nom: string,
    prenom: string,
    phone: string,
    email: string,
    experiences? : string[],
}