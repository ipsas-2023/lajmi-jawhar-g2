import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RightcolumnmidComponent } from './rightcolumnmid.component';

describe('RightcolumnmidComponent', () => {
  let component: RightcolumnmidComponent;
  let fixture: ComponentFixture<RightcolumnmidComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [RightcolumnmidComponent]
    });
    fixture = TestBed.createComponent(RightcolumnmidComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
