/* Créez une classe "Personne" avec des propriétés telles que le nom, l'âge, etc. 
Ajoutez ensuite des méthodes pour obtenir etdéfinir ces propriétés (exercice2.js). */

class Personne {
    constructor(nom,age,mail){
        this.nom = nom ;
        this.age = age ;
        this.mail = mail ;
    }

     afficheinfo (){
        console.log (`Le nom est ${this.nom}, il a ${this.age} et son mail est ${this.mail}`);

    }
}

const p1 = new Personne ("ali",12,"ali@ggg.com");
p1.afficheinfo();



