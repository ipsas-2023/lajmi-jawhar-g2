import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CvComponent } from './cv/cv.component';
import { CvleftcolumnComponent } from './cvleftcolumn/cvleftcolumn.component';
import { RightcolumnComponent } from './rightcolumn/rightcolumn.component';
import { RightcolumnmidComponent } from './rightcolumnmid/rightcolumnmid.component';
import { AutresComponent } from './autres/autres.component';
import { EtudesComponent } from './etudes/etudes.component';
import { CvFormComponent } from './cv-form/cv-form.component';
import { StoreModule } from '@ngrx/store';

@NgModule({
  declarations: [
    AppComponent,
    CvComponent,
    CvleftcolumnComponent,
    RightcolumnComponent,
    RightcolumnmidComponent,
    AutresComponent,
    EtudesComponent,
    CvFormComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    StoreModule.forRoot({}, {})
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
