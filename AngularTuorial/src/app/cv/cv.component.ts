import { Component, Input } from '@angular/core';
import { CVDATA } from './cv';

@Component({
  selector: 'app-cv-jawhar',
  templateUrl: './cv.component.html',
  styleUrls: ['./cv.component.scss']
})
export class CvComponent {
  @Input() cvdata1! : CVDATA;
  
  cvdata : CVDATA= {
    nom: "Jawhar",
    prenom: "Lajmi",
    phone: "1122334455",
    email: "jawhar.ipsas@hotmail.com",
    experiences: ["exp1","exp2","exp3"]
  }

}
