// TODO : Complétez la fonction pour qu'elle accepte un nombre indéterminé 
//d'arguments et renvoie leur somme.
function additionner(...nombres) {
    let somme = 0 ;
    for (let n of nombres){
        somme = somme + n ;
    }
    return somme ;
    // TODO : Remplissez le code ici pour calculer la somme des nombres
    }
    // Test de la fonction
    console.log(additionner(1, 2, 3, 4, 5)); // Attendu : 15
    console.log(additionner(10, 20, 30)); // Attendu : 60
    console.log(additionner(2, 4)); // Attendu : 6
    console.log(additionner(1)); // Attendu : 1
    console.log(additionner()); // Attendu : 0 (aucun argument)
    