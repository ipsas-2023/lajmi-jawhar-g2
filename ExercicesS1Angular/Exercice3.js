/* Utilisez les littéraux d'objet améliorés pour créer un objet "utilisateur" avec des propriétés telles que le nom, l'adresse e-mail,
etc.(exercice3.js).
*/

const nom = "Alice";
const email = "alice@email.com";
const age = 30;

const utilisateur = { 
    nom,
    email,
    age
}
 console.log (utilisateur);