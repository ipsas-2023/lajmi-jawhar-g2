import { Component, Input } from '@angular/core';
import { CVDATA } from '../cv/cv';


@Component({
  selector: 'app-cvleftcolumn',
  templateUrl: './cvleftcolumn.component.html',
  styleUrls: ['./cvleftcolumn.component.scss']
})
export class CvleftcolumnComponent {
@Input() cvdataleft! :CVDATA;
}
