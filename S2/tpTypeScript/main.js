var utilisateur = {
    nom: "Alice",
    age: 30,
    estConnecte: true,
    adresses: [
        { ville: "Paris", codePostal: 75000 },
        { ville: "New York", codePostal: 10001 },
    ],
};
console.log(utilisateur);
// TypeScript signale une erreur ici
