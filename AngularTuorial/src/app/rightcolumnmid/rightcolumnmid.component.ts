import { Component, Input } from '@angular/core';
import { CVDATA } from '../cv/cv';


@Component({
  selector: 'app-rightcolumnmid',
  templateUrl: './rightcolumnmid.component.html',
  styleUrls: ['./rightcolumnmid.component.scss']
})
export class RightcolumnmidComponent {
  @Input() cvDataRightmid!: CVDATA ;

}
