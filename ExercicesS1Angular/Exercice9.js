// TODO : Complétez la fonction pour concaténer deux
// tableaux en utilisant l'opérateur de diffusion.
function concatenateArrays(array1, array2) {
    return [...array1,...array2] ;
    // TODO : Remplissez le code ici pour concaténer les tableaux
    }
    // Test de la fonction
    const array1 = [1, 2, 3];
    const array2 = [4, 5, 6];
    const concatenatedArray = concatenateArrays(array1, array2);
    console.log(concatenatedArray); // Attendu : [1, 2, 3, 4, 5, 6]