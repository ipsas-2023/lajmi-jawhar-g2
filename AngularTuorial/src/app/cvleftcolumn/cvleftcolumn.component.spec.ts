import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CvleftcolumnComponent } from './cvleftcolumn.component';

describe('CvleftcolumnComponent', () => {
  let component: CvleftcolumnComponent;
  let fixture: ComponentFixture<CvleftcolumnComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CvleftcolumnComponent]
    });
    fixture = TestBed.createComponent(CvleftcolumnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
