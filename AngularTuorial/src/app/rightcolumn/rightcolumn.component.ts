import { Component, Input } from '@angular/core';
import { CVDATA } from '../cv/cv';

@Component({
  selector: 'app-rightcolumn',
  templateUrl: './rightcolumn.component.html',
  styleUrls: ['./rightcolumn.component.scss']
})
export class RightcolumnComponent {
  @Input() cvDataRight! : CVDATA; 
  
}
